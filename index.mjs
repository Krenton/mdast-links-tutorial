import fs from 'node:fs'
import {fromMarkdown} from 'mdast-util-from-markdown'

let markdown = fs.readFileSync('./lorem-ipsum.md');
let AST = fromMarkdown(markdown);
let linkObjectsArray = filterTree(AST, 'link');
console.log(linkObjectsArray.map(linkNode => linkNode.url));

function filterTree(tree, markdownNodeType){
  let requiredObjects = [];
  processNodes(tree);
  function processNodes(node){
    if (node.type === markdownNodeType) requiredObjects.push(node);
    if (node.children)node.children.forEach(processNodes);
  }
  return requiredObjects;
}
